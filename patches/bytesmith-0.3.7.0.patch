diff --git a/src/Data/Bytes/Parser.hs b/src/Data/Bytes/Parser.hs
index 1cf1dd8..0b559a7 100644
--- a/src/Data/Bytes/Parser.hs
+++ b/src/Data/Bytes/Parser.hs
@@ -1,5 +1,6 @@
 {-# language BangPatterns #-}
 {-# language BinaryLiterals #-}
+{-# language CPP #-}
 {-# language DataKinds #-}
 {-# language DeriveFunctor #-}
 {-# language DerivingStrategies #-}
@@ -206,12 +207,12 @@ bytes e !expected = Parser
 -- | Consume input matching the @NUL@-terminated C String.
 cstring :: e -> CString -> Parser e s ()
 cstring e (Exts.Ptr ptr0) = Parser
-  ( \(# arr, off0, len0 #) s -> 
-    let go !ptr !off !len = case Exts.indexWord8OffAddr# ptr 0# of
+  ( \(# arr, off0, len0 #) s ->
+    let go !ptr !off !len = case word8ToWordCompat# (Exts.indexWord8OffAddr# ptr 0#) of
           0## -> (# s, (# | (# (), off, len #) #) #)
           c -> case len of
             0# -> (# s, (# e | #) #)
-            _ -> case Exts.eqWord# c (Exts.indexWord8Array# arr off) of
+            _ -> case Exts.eqWord# c (word8ToWordCompat# (Exts.indexWord8Array# arr off)) of
               1# -> go (Exts.plusAddr# ptr 1# ) (off +# 1# ) (len -# 1# )
               _ -> (# s, (# e | #) #)
      in go ptr0 off0 len0
@@ -461,7 +462,7 @@ unboxWord32 (Parser f) = Parser
   (\x s0 -> case f x s0 of
     (# s1, r #) -> case r of
       (# e | #) -> (# s1, (# e | #) #)
-      (# | (# W32# a, b, c #) #) -> (# s1, (# | (# a, b, c #) #) #)
+      (# | (# W32# a, b, c #) #) -> (# s1, (# | (# word32ToWordCompat# a, b, c #) #) #)
   )
 
 -- | Convert a @(Int,Int)@ parser to a @(# Int#, Int# #)@ parser.
@@ -480,7 +481,7 @@ boxWord32 (Parser f) = Parser
   (\x s0 -> case f x s0 of
     (# s1, r #) -> case r of
       (# e | #) -> (# s1, (# e | #) #)
-      (# | (# a, b, c #) #) -> (# s1, (# | (# W32# a, b, c #) #) #)
+      (# | (# a, b, c #) #) -> (# s1, (# | (# W32# (wordToWord32Compat# a), b, c #) #) #)
   )
 
 -- | Convert a @(# Int#, Int# #)@ parser to a @(Int,Int)@ parser.
@@ -697,3 +698,23 @@ replicate !len p = do
 
 unI :: Int -> Int#
 unI (I# w) = w
+
+#if MIN_VERSION_base(4,16,0)
+word8ToWordCompat# :: Exts.Word8# -> Word#
+word8ToWordCompat# = Exts.word8ToWord#
+
+word32ToWordCompat# :: Exts.Word32# -> Word#
+word32ToWordCompat# = Exts.word32ToWord#
+
+wordToWord32Compat# :: Word# -> Exts.Word32#
+wordToWord32Compat# = Exts.wordToWord32#
+#else
+word8ToWordCompat# :: Word# -> Word#
+word8ToWordCompat# x = x
+
+word32ToWordCompat# :: Word# -> Word#
+word32ToWordCompat# x = x
+
+wordToWord32Compat# :: Word# -> Word#
+wordToWord32Compat# x = x
+#endif
diff --git a/src/Data/Bytes/Parser/Latin.hs b/src/Data/Bytes/Parser/Latin.hs
index 161b774..75c3191 100644
--- a/src/Data/Bytes/Parser/Latin.hs
+++ b/src/Data/Bytes/Parser/Latin.hs
@@ -1,5 +1,6 @@
 {-# language BangPatterns #-}
 {-# language BinaryLiterals #-}
+{-# language CPP #-}
 {-# language DataKinds #-}
 {-# language DeriveFunctor #-}
 {-# language DerivingStrategies #-}
@@ -372,7 +373,7 @@ skipDigitsAscii1LoopStart ::
   -> Bytes -- chunk
   -> Result# e ()
 skipDigitsAscii1LoopStart e !c = if length c > 0
-  then 
+  then
     let w = indexLatinCharArray (array c) (offset c)
      in if w >= '0' && w <= '9'
           then upcastUnitSuccess (skipDigitsAsciiLoop (Bytes.unsafeDrop 1 c))
@@ -600,17 +601,17 @@ decWordStart e !chunk0 s0 = if length chunk0 > 0
 -- Precondition: the word is small enough
 upcastWord16Result :: Result# e Word# -> Result# e Word16
 upcastWord16Result (# e | #) = (# e | #)
-upcastWord16Result (# | (# a, b, c #) #) = (# | (# W16# a, b, c #) #)
+upcastWord16Result (# | (# a, b, c #) #) = (# | (# W16# (wordToWord16Compat# a), b, c #) #)
 
 -- Precondition: the word is small enough
 upcastWord32Result :: Result# e Word# -> Result# e Word32
 upcastWord32Result (# e | #) = (# e | #)
-upcastWord32Result (# | (# a, b, c #) #) = (# | (# W32# a, b, c #) #)
+upcastWord32Result (# | (# a, b, c #) #) = (# | (# W32# (wordToWord32Compat# a), b, c #) #)
 
 -- Precondition: the word is small enough
 upcastWord8Result :: Result# e Word# -> Result# e Word8
 upcastWord8Result (# e | #) = (# e | #)
-upcastWord8Result (# | (# a, b, c #) #) = (# | (# W8# a, b, c #) #)
+upcastWord8Result (# | (# a, b, c #) #) = (# | (# W8# (wordToWord8Compat# a), b, c #) #)
 
 -- | Parse a decimal-encoded number. If the number is too large to be
 -- represented by a machine integer, this fails with the provided
@@ -686,7 +687,7 @@ decSignedInt# e = any e `bindFromLiftedToInt` \c -> case c of
     (\chunk0 s0 -> decPosIntStart e (boxBytes chunk0) s0)
   '-' -> Parser -- minus sign
     (\chunk0 s0 -> decNegIntStart e (boxBytes chunk0) s0)
-  _ -> Parser -- no sign, there should be a digit here 
+  _ -> Parser -- no sign, there should be a digit here
     (\chunk0 s0 ->
       let !w = char2Word c - 48
         in if w < 10
@@ -701,7 +702,7 @@ decStandardInt# :: e -> Parser e s Int#
 decStandardInt# e = any e `bindFromLiftedToInt` \c -> case c of
   '-' -> Parser -- minus sign
     (\chunk0 s0 -> decNegIntStart e (boxBytes chunk0) s0)
-  _ -> Parser -- no sign, there should be a digit here 
+  _ -> Parser -- no sign, there should be a digit here
     (\chunk0 s0 ->
       let !w = char2Word c - 48
         in if w < 10
@@ -738,7 +739,7 @@ decSignedInteger e = any e >>= \c -> case c of
   '-' -> do
     x <- decUnsignedInteger e
     pure $! negate x
-  _ -> Parser -- no sign, there should be a digit here 
+  _ -> Parser -- no sign, there should be a digit here
     (\chunk0 s0 ->
       let !w = char2Word c - 48 in
       if w < 10
@@ -774,11 +775,11 @@ decNegIntStart e !chunk0 s0 = if length chunk0 > 0
     let !w = fromIntegral @Word8 @Word
           (PM.indexByteArray (array chunk0) (offset chunk0)) - 48
      in if w < 10
-          then 
+          then
             case decPosIntMore e w (maxIntAsWord + 1) (Bytes.unsafeDrop 1 chunk0) of
-             (# | (# x, y, z #) #) -> 
+             (# | (# x, y, z #) #) ->
                (# s0, (# | (# (notI# x +# 1# ), y, z #) #) #)
-             (# err | #) -> 
+             (# err | #) ->
                (# s0, (# err | #) #)
           else (# s0, (# e | #) #)
   else (# s0, (# e | #) #)
@@ -806,7 +807,7 @@ decUnsignedIntegerStart e !chunk0 s0 = if length chunk0 > 0
 -- exceeds the upper bound.
 decPosIntMore ::
      e -- Error message
-  -> Word -- Accumulator, precondition: less than or equal to bound 
+  -> Word -- Accumulator, precondition: less than or equal to bound
   -> Word -- Inclusive Upper Bound, either (2^63 - 1) or 2^63
   -> Bytes -- Chunk
   -> Result# e Int#
@@ -897,7 +898,7 @@ takeTrailedBy e !w = do
 -- | Skip all characters until the terminator is encountered
 -- and then consume the matching character as well. Visually,
 -- @skipTrailedBy \'C\'@ advances the cursor like this:
--- 
+--
 -- >  A Z B Y C X C W
 -- > |->->->->-|
 --
@@ -910,7 +911,7 @@ skipTrailedBy e !w = uneffectful# $ \c ->
 -- | Skip all characters until the terminator is encountered.
 -- This does not consume the terminator. Visually, @skipUntil \'C\'@
 -- advances the cursor like this:
--- 
+--
 -- >  A Z B Y C X C W
 -- > |->->->-|
 --
@@ -950,7 +951,7 @@ hexFixedWord32 e = Parser
   (\x s0 -> case runParser (hexFixedWord32# e) x s0 of
     (# s1, r #) -> case r of
       (# err | #) -> (# s1, (# err | #) #)
-      (# | (# a, b, c #) #) -> (# s1, (# | (# W32# a, b, c #) #) #)
+      (# | (# a, b, c #) #) -> (# s1, (# | (# W32# (wordToWord32Compat# a), b, c #) #) #)
   )
 
 hexFixedWord32# :: e -> Parser e s Word#
@@ -999,7 +1000,7 @@ hexFixedWord64# e = uneffectfulWord# $ \chunk -> if length chunk >= 16
   then
     let go !off !len !acc = case len of
           0 -> case acc of
-            W# r -> 
+            W# r ->
               (# | (# r
               ,  unI off
               ,  unI (length chunk) -# 16# #) #)
@@ -1021,7 +1022,7 @@ hexFixedWord16 e = Parser
   (\x s0 -> case runParser (hexFixedWord16# e) x s0 of
     (# s1, r #) -> case r of
       (# err | #) -> (# s1, (# err | #) #)
-      (# | (# a, b, c #) #) -> (# s1, (# | (# W16# a, b, c #) #) #)
+      (# | (# a, b, c #) #) -> (# s1, (# | (# W16# (wordToWord16Compat# a), b, c #) #) #)
   )
 
 hexFixedWord16# :: e -> Parser e s Word#
@@ -1053,7 +1054,7 @@ hexFixedWord8 e = Parser
   (\x s0 -> case runParser (hexFixedWord8# e) x s0 of
     (# s1, r #) -> case r of
       (# err | #) -> (# s1, (# err | #) #)
-      (# | (# a, b, c #) #) -> (# s1, (# | (# W8# a, b, c #) #) #)
+      (# | (# a, b, c #) #) -> (# s1, (# | (# W8# (wordToWord8Compat# a), b, c #) #) #)
   )
 
 hexFixedWord8# :: e -> Parser e s Word#
@@ -1149,7 +1150,7 @@ uneffectfulWord# f = Parser
 -- Postcondition: when overflow is false, the resulting
 -- word is less than or equal to the upper bound
 positivePushBase10 :: Word -> Word -> Word -> (Bool,Word)
-positivePushBase10 (W# a) (W# b) (W# upper) = 
+positivePushBase10 (W# a) (W# b) (W# upper) =
   let !(# ca, r0 #) = Exts.timesWord2# a 10##
       !r1 = Exts.plusWord# r0 b
       !cb = int2Word# (gtWord# r1 upper)
@@ -1158,7 +1159,7 @@ positivePushBase10 (W# a) (W# b) (W# upper) =
    in (case c of { 0## -> False; _ -> True }, W# r1)
 
 unsignedPushBase10 :: Word -> Word -> (Bool,Word)
-unsignedPushBase10 (W# a) (W# b) = 
+unsignedPushBase10 (W# a) (W# b) =
   let !(# ca, r0 #) = Exts.timesWord2# a 10##
       !r1 = Exts.plusWord# r0 b
       !cb = int2Word# (ltWord# r1 r0)
@@ -1189,3 +1190,23 @@ anyUnsafe = uneffectful $ \chunk ->
 -- Reads one byte and interprets it as Latin1-encoded character.
 indexCharArray :: PM.ByteArray -> Int -> Char
 indexCharArray (PM.ByteArray x) (I# i) = C# (indexCharArray# x i)
+
+#if MIN_VERSION_base(4,16,0)
+wordToWord8Compat# :: Word# -> Exts.Word8#
+wordToWord8Compat# = Exts.wordToWord8#
+
+wordToWord16Compat# :: Word# -> Exts.Word16#
+wordToWord16Compat# = Exts.wordToWord16#
+
+wordToWord32Compat# :: Word# -> Exts.Word32#
+wordToWord32Compat# = Exts.wordToWord32#
+#else
+wordToWord8Compat# :: Word# -> Word#
+wordToWord8Compat# x = x
+
+wordToWord16Compat# :: Word# -> Word#
+wordToWord16Compat# x = x
+
+wordToWord32Compat# :: Word# -> Word#
+wordToWord32Compat# x = x
+#endif
diff --git a/src/Data/Bytes/Parser/Rebindable.hs b/src/Data/Bytes/Parser/Rebindable.hs
index 04f295f..5aa3437 100644
--- a/src/Data/Bytes/Parser/Rebindable.hs
+++ b/src/Data/Bytes/Parser/Rebindable.hs
@@ -1,3 +1,4 @@
+{-# language CPP #-}
 {-# language FlexibleInstances #-}
 {-# language MagicHash #-}
 {-# language MultiParamTypeClasses #-}
@@ -21,8 +22,12 @@ module Data.Bytes.Parser.Rebindable
   , Pure(..)
   ) where
 
-import Prelude () 
-import GHC.Exts (TYPE,RuntimeRep(..))
+import Prelude ()
+import GHC.Exts (TYPE,RuntimeRep(..)
+#if MIN_VERSION_base(4,16,0)
+                ,LiftedRep
+#endif
+                )
 import Data.Bytes.Parser.Internal (Parser(..))
 
 class Bind (ra :: RuntimeRep) (rb :: RuntimeRep) where
@@ -139,8 +144,8 @@ sequenceIntPairParser (Parser f) (Parser g) = Parser
       (# | (# _, b, c #) #) -> g (# arr, b, c #) s1
   )
 
-bindInt2to5Parser :: forall 
-  (a :: TYPE ('TupleRep '[ 'IntRep, 'IntRep])) 
+bindInt2to5Parser :: forall
+  (a :: TYPE ('TupleRep '[ 'IntRep, 'IntRep]))
   (b :: TYPE ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep]))
   e s.
   Parser e s a -> (a -> Parser e s b) -> Parser e s b
@@ -153,7 +158,7 @@ bindInt2to5Parser (Parser f) g = Parser
         runParser (g y) (# arr, b, c #) s1
   )
 
-sequenceInt2to5Parser :: forall 
+sequenceInt2to5Parser :: forall
   (a :: TYPE ('TupleRep '[ 'IntRep, 'IntRep]))
   (b :: TYPE ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep]))
   e s.
@@ -166,19 +171,19 @@ sequenceInt2to5Parser (Parser f) (Parser g) = Parser
       (# | (# _, b, c #) #) -> g (# arr, b, c #) s1
   )
 
-instance Bind 'LiftedRep 'LiftedRep where
+instance Bind LiftedRep LiftedRep where
   {-# inline (>>=) #-}
   {-# inline (>>) #-}
   (>>=) = bindParser
   (>>) = sequenceParser
 
-instance Bind 'IntRep 'LiftedRep where
+instance Bind 'IntRep LiftedRep where
   {-# inline (>>=) #-}
   {-# inline (>>) #-}
   (>>=) = bindIntParser
   (>>) = sequenceIntParser
 
-instance Bind ('TupleRep '[ 'IntRep, 'IntRep]) 'LiftedRep where
+instance Bind ('TupleRep '[ 'IntRep, 'IntRep]) LiftedRep where
   {-# inline (>>=) #-}
   {-# inline (>>) #-}
   (>>=) = bindIntPairParser
@@ -186,15 +191,15 @@ instance Bind ('TupleRep '[ 'IntRep, 'IntRep]) 'LiftedRep where
 
 
 instance Bind ('TupleRep '[ 'IntRep, 'IntRep])
-              ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep]) 
+              ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep])
   where
   {-# inline (>>=) #-}
   {-# inline (>>) #-}
   (>>=) = bindInt2to5Parser
   (>>) = sequenceInt2to5Parser
 
-instance Bind ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep]) 
-              'LiftedRep
+instance Bind ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep])
+              LiftedRep
   where
   {-# inline (>>=) #-}
   {-# inline (>>) #-}
@@ -203,21 +208,21 @@ instance Bind ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep])
 
 
 instance Bind 'IntRep
-              ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep]) 
+              ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep])
   where
   {-# inline (>>=) #-}
   {-# inline (>>) #-}
   (>>=) = bindFromIntToInt5
   (>>) = sequenceIntToInt5
 
-instance Bind 'LiftedRep ('TupleRep '[ 'IntRep, 'IntRep]) where
+instance Bind LiftedRep ('TupleRep '[ 'IntRep, 'IntRep]) where
   {-# inline (>>=) #-}
   {-# inline (>>) #-}
   (>>=) = bindFromLiftedToIntPair
   (>>) = sequenceLiftedToIntPair
 
-instance Bind 'LiftedRep
-              ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep]) 
+instance Bind LiftedRep
+              ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep])
   where
   {-# inline (>>=) #-}
   {-# inline (>>) #-}
@@ -230,13 +235,13 @@ instance Bind 'IntRep ('TupleRep '[ 'IntRep, 'IntRep]) where
   (>>=) = bindFromIntToIntPair
   (>>) = sequenceIntToIntPair
 
-instance Bind 'LiftedRep 'IntRep where
+instance Bind LiftedRep 'IntRep where
   {-# inline (>>=) #-}
   {-# inline (>>) #-}
   (>>=) = bindFromLiftedToInt
   (>>) = sequenceLiftedToInt
 
-instance Pure 'LiftedRep where
+instance Pure LiftedRep where
   {-# inline pure #-}
   pure = pureParser
 
@@ -316,7 +321,7 @@ sequenceIntToInt5 (Parser f) (Parser g) = Parser
 
 bindFromLiftedToIntPair ::
      forall s e
-       (a :: TYPE 'LiftedRep)
+       (a :: TYPE LiftedRep)
        (b :: TYPE ('TupleRep '[ 'IntRep, 'IntRep ])).
      Parser s e a
   -> (a -> Parser s e b)
@@ -332,7 +337,7 @@ bindFromLiftedToIntPair (Parser f) g = Parser
 
 sequenceLiftedToIntPair ::
      forall s e
-       (a :: TYPE 'LiftedRep)
+       (a :: TYPE LiftedRep)
        (b :: TYPE ('TupleRep '[ 'IntRep, 'IntRep ])).
      Parser s e a
   -> Parser s e b
@@ -348,7 +353,7 @@ sequenceLiftedToIntPair (Parser f) (Parser g) = Parser
 
 bindFromLiftedToInt5 ::
      forall s e
-       (a :: TYPE 'LiftedRep)
+       (a :: TYPE LiftedRep)
        (b :: TYPE ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep])).
      Parser s e a
   -> (a -> Parser s e b)
@@ -364,7 +369,7 @@ bindFromLiftedToInt5 (Parser f) g = Parser
 
 sequenceLiftedToInt5 ::
      forall s e
-       (a :: TYPE 'LiftedRep)
+       (a :: TYPE LiftedRep)
        (b :: TYPE ('TupleRep '[ 'IntRep, 'IntRep, 'IntRep, 'IntRep, 'IntRep ])).
      Parser s e a
   -> Parser s e b
@@ -379,7 +384,7 @@ sequenceLiftedToInt5 (Parser f) (Parser g) = Parser
 
 bindFromLiftedToInt ::
      forall s e
-       (a :: TYPE 'LiftedRep)
+       (a :: TYPE LiftedRep)
        (b :: TYPE 'IntRep).
      Parser s e a
   -> (a -> Parser s e b)
@@ -395,7 +400,7 @@ bindFromLiftedToInt (Parser f) g = Parser
 
 sequenceLiftedToInt ::
      forall s e
-       (a :: TYPE 'LiftedRep)
+       (a :: TYPE LiftedRep)
        (b :: TYPE 'IntRep).
      Parser s e a
   -> Parser s e b
diff --git a/src/Data/Bytes/Parser/Utf8.hs b/src/Data/Bytes/Parser/Utf8.hs
index 779009a..44b8997 100644
--- a/src/Data/Bytes/Parser/Utf8.hs
+++ b/src/Data/Bytes/Parser/Utf8.hs
@@ -1,5 +1,6 @@
 {-# language BangPatterns #-}
 {-# language BinaryLiterals #-}
+{-# language CPP #-}
 {-# language DataKinds #-}
 {-# language DeriveFunctor #-}
 {-# language DerivingStrategies #-}
@@ -46,8 +47,8 @@ any# e = Parser
   (\(# arr, off, len #) s0 -> case len ># 0# of
     1# ->
       let !w0 = Exts.indexWord8Array# arr off
-       in if | oneByteChar (W8# w0) -> 
-                 (# s0, (# | (# chr# (Exts.word2Int# w0), off +# 1#, len -# 1# #) #) #)
+       in if | oneByteChar (W8# w0) ->
+                 (# s0, (# | (# chr# (Exts.word2Int# (word8ToWordCompat# w0)), off +# 1#, len -# 1# #) #) #)
              | twoByteChar (W8# w0) ->
                  if | I# len > 1
                     , w1 <- Exts.indexWord8Array# arr (off +# 1#)
@@ -81,9 +82,9 @@ codepointFromFourBytes :: Word8 -> Word8 -> Word8 -> Word8 -> Char
 codepointFromFourBytes w1 w2 w3 w4 = C#
   ( chr#
     ( unI $ fromIntegral
-      ( unsafeShiftL (word8ToWord w1 .&. 0b00001111) 18 .|. 
-        unsafeShiftL (word8ToWord w2 .&. 0b00111111) 12 .|. 
-        unsafeShiftL (word8ToWord w3 .&. 0b00111111) 6 .|. 
+      ( unsafeShiftL (word8ToWord w1 .&. 0b00001111) 18 .|.
+        unsafeShiftL (word8ToWord w2 .&. 0b00111111) 12 .|.
+        unsafeShiftL (word8ToWord w3 .&. 0b00111111) 6 .|.
         (word8ToWord w4 .&. 0b00111111)
       )
     )
@@ -93,8 +94,8 @@ codepointFromThreeBytes :: Word8 -> Word8 -> Word8 -> Char
 codepointFromThreeBytes w1 w2 w3 = C#
   ( chr#
     ( unI $ fromIntegral
-      ( unsafeShiftL (word8ToWord w1 .&. 0b00001111) 12 .|. 
-        unsafeShiftL (word8ToWord w2 .&. 0b00111111) 6 .|. 
+      ( unsafeShiftL (word8ToWord w1 .&. 0b00001111) 12 .|.
+        unsafeShiftL (word8ToWord w2 .&. 0b00111111) 6 .|.
         (word8ToWord w3 .&. 0b00111111)
       )
     )
@@ -104,7 +105,7 @@ codepointFromTwoBytes :: Word8 -> Word8 -> Char
 codepointFromTwoBytes w1 w2 = C#
   ( chr#
     ( unI $ fromIntegral @Word @Int
-      ( unsafeShiftL (word8ToWord w1 .&. 0b00011111) 6 .|. 
+      ( unsafeShiftL (word8ToWord w1 .&. 0b00011111) 6 .|.
         (word8ToWord w2 .&. 0b00111111)
       )
     )
@@ -141,3 +142,11 @@ shortByteStringToByteArray ::
      BSS.ShortByteString
   -> PM.ByteArray
 shortByteStringToByteArray (BSS.SBS x) = PM.ByteArray x
+
+#if MIN_VERSION_base(4,16,0)
+word8ToWordCompat# :: Exts.Word8# -> Exts.Word#
+word8ToWordCompat# = Exts.word8ToWord#
+#else
+word8ToWordCompat# :: Exts.Word# -> Exts.Word#
+word8ToWordCompat# x = x
+#endif
