diff --git a/diagrams-lib.cabal b/diagrams-lib.cabal
index 0ee8824..68dc0fa 100644
--- a/diagrams-lib.cabal
+++ b/diagrams-lib.cabal
@@ -1,5 +1,6 @@
 Name:                diagrams-lib
 Version:             1.4.3
+x-revision: 1
 Synopsis:            Embedded domain-specific language for declarative graphics
 Description:         Diagrams is a flexible, extensible EDSL for creating
                      graphics of many types.  Graphics can be created
@@ -113,13 +114,13 @@ Library
                        data-default-class < 0.2,
                        fingertree >= 0.1 && < 0.2,
                        intervals >= 0.7 && < 0.10,
-                       lens >= 4.6 && < 4.19,
+                       lens >= 4.6 && < 4.20,
                        tagged >= 0.7,
                        optparse-applicative >= 0.11 && < 0.16,
                        filepath,
                        JuicyPixels >= 3.3.4 && < 3.4,
                        hashable >= 1.1 && < 1.4,
-                       linear >= 1.20.1 && < 1.21,
+                       linear >= 1.20.1 && < 1.22,
                        adjunctions >= 4.0 && < 5.0,
                        distributive >=0.2.2 && < 1.0,
                        process >= 1.1 && < 1.7,
diff --git a/src/Diagrams/BoundingBox.hs b/src/Diagrams/BoundingBox.hs
index a4dce3b..e082d68 100644
--- a/src/Diagrams/BoundingBox.hs
+++ b/src/Diagrams/BoundingBox.hs
@@ -80,7 +80,7 @@ type instance V (NonEmptyBoundingBox v n) = v
 type instance N (NonEmptyBoundingBox v n) = n
 
 fromNonEmpty :: NonEmptyBoundingBox v n -> BoundingBox v n
-fromNonEmpty = BoundingBox . Option . Just
+fromNonEmpty = BoundingBox . Just
 
 fromMaybeEmpty :: Maybe (NonEmptyBoundingBox v n) -> BoundingBox v n
 fromMaybeEmpty = maybe emptyBox fromNonEmpty
@@ -95,7 +95,7 @@ instance (Additive v, Ord n) => Semigroup (NonEmptyBoundingBox v n) where
 -- | A bounding box is an axis-aligned region determined by two points
 --   indicating its \"lower\" and \"upper\" corners.  It can also represent
 --   an empty bounding box - the points are wrapped in @Maybe@.
-newtype BoundingBox v n = BoundingBox (Option (NonEmptyBoundingBox v n))
+newtype BoundingBox v n = BoundingBox (Maybe (NonEmptyBoundingBox v n))
   deriving (Eq, Functor)
 
 deriving instance (Additive v, Ord n) => Semigroup (BoundingBox v n)
@@ -166,7 +166,7 @@ instance Read (v n) => Read (BoundingBox v n) where
 -- | An empty bounding box.  This is the same thing as @mempty@, but it doesn't
 --   require the same type constraints that the @Monoid@ instance does.
 emptyBox :: BoundingBox v n
-emptyBox = BoundingBox $ Option Nothing
+emptyBox = BoundingBox Nothing
 
 -- | Create a bounding box from a point that is component-wise @(<=)@ than the
 --   other.  If this is not the case, then @mempty@ is returned.
@@ -196,17 +196,17 @@ boundingBox a = fromMaybeEmpty $ do
 
 -- | Queries whether the BoundingBox is empty.
 isEmptyBox :: BoundingBox v n -> Bool
-isEmptyBox (BoundingBox (Option Nothing)) = True
-isEmptyBox _                              = False
+isEmptyBox (BoundingBox Nothing) = True
+isEmptyBox _                     = False
 
 -- | Gets the lower and upper corners that define the bounding box.
 getCorners :: BoundingBox v n -> Maybe (Point v n, Point v n)
-getCorners (BoundingBox p) = nonEmptyCorners <$> getOption p
+getCorners (BoundingBox p) = nonEmptyCorners <$> p
 
 -- | Computes all of the corners of the bounding box.
 getAllCorners :: (Additive v, Traversable v) => BoundingBox v n -> [Point v n]
-getAllCorners (BoundingBox (Option Nothing)) = []
-getAllCorners (BoundingBox (Option (Just (NonEmptyBoundingBox (l, u)))))
+getAllCorners (BoundingBox Nothing) = []
+getAllCorners (BoundingBox (Just (NonEmptyBoundingBox (l, u))))
   = T.sequence (liftI2 (\a b -> [a,b]) l u)
 
 -- | Get the size of the bounding box - the vector from the (component-wise)
diff --git a/src/Diagrams/Combinators.hs b/src/Diagrams/Combinators.hs
index 82387b3..6e41fd1 100644
--- a/src/Diagrams/Combinators.hs
+++ b/src/Diagrams/Combinators.hs
@@ -165,7 +165,7 @@ deformEnvelope
   => n -> v n -> QDiagram b v n m -> QDiagram b v n m
 deformEnvelope s v = over (envelope . _Wrapping Envelope) deformE
   where
-    deformE = Option . fmap deformE' . getOption
+    deformE = fmap deformE'
     deformE' env v'
         | dp > 0    = Max $ getMax (env v') + (dp * s) / quadrance v'
         | otherwise = env v'
diff --git a/src/Diagrams/CubicSpline/Internal.hs b/src/Diagrams/CubicSpline/Internal.hs
index 1087d4e..08c09f6 100644
--- a/src/Diagrams/CubicSpline/Internal.hs
+++ b/src/Diagrams/CubicSpline/Internal.hs
@@ -21,7 +21,7 @@ module Diagrams.CubicSpline.Internal
 
 import           Diagrams.Solve.Tridiagonal
 
-import           Data.List
+import           Data.List (zip4)
 
 -- | Use the tri-diagonal solver with the appropriate parameters for an open cubic spline.
 solveCubicSplineDerivatives :: Fractional a => [a] -> [a]
diff --git a/src/Diagrams/Trail.hs b/src/Diagrams/Trail.hs
index d5b152a..c65fa96 100644
--- a/src/Diagrams/Trail.hs
+++ b/src/Diagrams/Trail.hs
@@ -311,7 +311,7 @@ instance (Metric v, OrderedField n, Real n)
 --   (along with a default value) to a function on an entire trail.
 trailMeasure :: ( SegMeasure v n :>: m, FT.Measured (SegMeasure v n) t )
              => a -> (m -> a) -> t -> a
-trailMeasure d f = option d f . get . FT.measure
+trailMeasure d f = maybe d f . get . FT.measure
 
 -- | Compute the number of segments of anything measured by
 --   'SegMeasure' (/e.g./ @SegMeasure@ itself, @Segment@, @SegTree@,
diff --git a/src/Diagrams/Transform.hs b/src/Diagrams/Transform.hs
index 1f29243..36dbb69 100644
--- a/src/Diagrams/Transform.hs
+++ b/src/Diagrams/Transform.hs
@@ -125,4 +125,4 @@ movedFrom p = iso (moveOriginTo (negated p)) (moveOriginTo p)
 -- @
 translated :: (InSpace v n a, SameSpace a b, Transformable a, Transformable b)
            => v n -> Iso a b a b
-translated = transformed . translation
+translated v = transformed $ translation v
diff --git a/src/Diagrams/Transform/Matrix.hs b/src/Diagrams/Transform/Matrix.hs
index 4bba99b..8aba3e2 100644
--- a/src/Diagrams/Transform/Matrix.hs
+++ b/src/Diagrams/Transform/Matrix.hs
@@ -30,7 +30,7 @@ import           Linear.Vector
 
 -- | Build a matrix from a 'Transformation', ignoring the translation.
 mkMat :: (HasBasis v, Num n) => Transformation v n -> v (v n)
-mkMat t = distribute . tabulate $ apply t . unit . el
+mkMat t = distribute . tabulate $ \x -> apply t $ unit $ el x
 
 -- | Build a 3D transformation matrix in homogeneous coordinates from
 --   a 'Transformation V3'.
diff --git a/src/Diagrams/TwoD/Arrow.hs b/src/Diagrams/TwoD/Arrow.hs
index 3465b31..b40d38b 100644
--- a/src/Diagrams/TwoD/Arrow.hs
+++ b/src/Diagrams/TwoD/Arrow.hs
@@ -401,7 +401,7 @@ arrow' opts len = mkQD' (DelayedLeaf delayedArrow)
     -- uniformly as the transformation applied to the entire arrow.
     -- See https://github.com/diagrams/diagrams-lib/issues/112.
     delayedArrow da g n =
-      let (trans, globalSty) = option mempty untangle . fst $ da
+      let (trans, globalSty) = maybe mempty untangle . fst $ da
       in  dArrow globalSty trans len g n
 
     -- Build an arrow and set its endpoints to the image under tr of origin and (len,0).
diff --git a/src/Diagrams/TwoD/Transform.hs b/src/Diagrams/TwoD/Transform.hs
index c328202..1a26b5d 100644
--- a/src/Diagrams/TwoD/Transform.hs
+++ b/src/Diagrams/TwoD/Transform.hs
@@ -98,7 +98,7 @@ rotateBy = transform . rotation . review turn
 -- @
 rotated :: (InSpace V2 n a, Floating n, SameSpace a b, Transformable a, Transformable b)
         => Angle n -> Iso a b a b
-rotated = transformed . rotation
+rotated a = transformed $ rotation a
 
 -- | @rotationAbout p@ is a rotation about the point @p@ (instead of
 --   around the local origin).
diff --git a/src/Diagrams/Util.hs b/src/Diagrams/Util.hs
index 5207c72..12f4e81 100644
--- a/src/Diagrams/Util.hs
+++ b/src/Diagrams/Util.hs
@@ -40,7 +40,7 @@ import           Control.Monad.Catch
 import           Control.Monad.Trans
 import           Control.Monad.Trans.Maybe
 import           Data.Default.Class
-import           Data.List
+import           Data.List (find, isSuffixOf)
 import           Data.List.Lens
 import           Data.Maybe
 import           Data.Monoid
