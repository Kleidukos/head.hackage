#!/usr/bin/env bash

set -e

project_id=$1
pipeline_id=$2
job_name=$3

# Access token is a protected environment variable in the head.hackage project and
# is necessary for this query to succeed. Sadly job tokens only seem to
# give us access to the project being built.
curl \
  --silent --show-error \
  -H "Private-Token: $ACCESS_TOKEN" \
  "https://gitlab.haskell.org/api/v4/projects/$project_id/pipelines/$pipeline_id/jobs?scope[]=success" \
  > resp.json

job_id=$(jq ". | map(select(.name == \"$job_name\")) | .[0].id" < resp.json)
if [ "$job_id" = "null" ]; then
  echo "Error finding job $job_name for $pipeline_id in project $project_id:" >&2
  cat resp.json >&2
  rm resp.json
  exit 1
else
  rm resp.json
  echo -n "$job_id"
fi
