let
  sources = import ./nix/sources.nix;
  nixpkgs = import sources.nixpkgs {};
in

{ ghcTarball }:
let
  nixpkgsSrc = sources.nixpkgs;
  ghc-artefact-nix =
    let
      src = nixpkgs.fetchFromGitHub {
        owner = "mpickering";
        repo = "ghc-artefact-nix";
        rev = "5da00a2f9411f15a7e0cbacc0ad94ff4ffa195d7";
        sha256 = "sha256:1ab2hc893vlsclddqyvw3yrhg33sn7pjckl2v8yi67d2jwc9zygn";
      };
    in import src {
      url = ghcTarball;
      nixpkgsSrc = sources.nixpkgs;
    };
in ghc-artefact-nix.ghcHEAD
