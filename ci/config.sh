# vi: set filetype=sh

# Packages expected not to build due to GHC bugs. This is `source`'d by the CI
# script and the arguments in BROKEN_ARGS are added to the hackage-ci
# command-line.

# Mark the named package as broken.
#
# Usage:
#    broken $pkg_name $ghc_ticket_number
#
function broken() {
  pkg_name="$1"
  ticket="$2"
  echo "Marking $pkg_name as broken due to #$ticket"
  EXTRA_OPTS="$EXTRA_OPTS --expect-broken=$pkg_name"
}

function only_package() {
  echo "Adding $@ to --only package list"
  for pkg in $@; do
    EXTRA_OPTS="$EXTRA_OPTS --only=$pkg"
  done
}

# Return the version number of the most recent release of the given package
function latest_version() {
  pkg=$1
  curl -s -H "Accept: application/json" -L -X GET http://hackage.haskell.org/package/$pkg/preferred | jq '.["normal-version"] | .[0]' -r
}

# Add a package to the set of packages that lack patches but are nevertheless
# tested.
function extra_package() {
  pkg_name="$1"
  version="$2"
  if [ -z "$version" ]; then
    version=$(latest_version $pkg_name)
  fi
  echo "Adding $pkg_name-$version to extra package set"
  EXTRA_OPTS="$EXTRA_OPTS --extra-package=$pkg_name==$version"
}

# Mark a package to be declared with build-tool-depends, not build-depends.
# This is necessary for packages that do not have a library component.
function build_tool_package() {
  pkg_name="$1"
  echo "Adding $pkg_name as a build-tool package"
  EXTRA_OPTS="$EXTRA_OPTS --build-tool-package=$pkg_name"
}

if [ -z "$GHC" ]; then GHC=ghc; fi

function ghc_version() {
  $GHC --version | sed 's/.*version \([0-9]*\.\([0-9]*\.\)*\)/\1/'
}

function ghc_commit() {
  $GHC --print-project-git-commit-id
}

# ======================================================================
# The lists begin here
#
# For instance:
#
#    broken "lens" 17988

version="$(ghc_version)"
commit="$(ghc_commit)"
echo "Found GHC $version, commit $commit."
case $version in
  9.0.*)
    #       package             ticket
    ;;

  9.2.*)
    #       package             ticket
    ;;

  9.3.*)
    #       package             ticket
    ;;

  *)
    echo "No broken packages for GHC $version"
    ;;
esac

# Extra packages
# ==============
#
# These are packages which we don't have patches for but want to test anyways.
extra_package lens
extra_package aeson
extra_package criterion
extra_package scotty
extra_package generic-lens
extra_package microstache
extra_package singletons
extra_package servant
extra_package hgmp

# Build-tool packages
build_tool_package alex
build_tool_package happy

# Quick build configuration
# =========================
#
# If $QUICK is defined we build the "quick" configuration, which builds a small
# subset of the overall package set. We do this during the merge request
# validation pipeline.
if [ -n "$QUICK" ]; then
  only_package Cabal
  only_package microlens
  only_package free
  only_package optparse-applicative
fi
